package me.danvd.andrew.postandroidkotlin

import io.reactivex.subjects.PublishSubject

typealias Posts = List<Post>

typealias PostsCollectionNotification = Pair<PostsCollectionCache, List<Int> /* affected indices */>

class PostsCollectionCache {
    private val mCachedPosts = mutableListOf<Post>()
    private val mNotifications = PublishSubject.create<PostsCollectionNotification>()

    fun notifications() = mNotifications

    private fun postChangedNotification(affectedIndices: List<Int>) {
        this.mNotifications.onNext(PostsCollectionNotification(this, affectedIndices))
    }

    fun addPost(post: Post, atIndex: Int?) {
        this.mCachedPosts.remove(post)
        if (atIndex == null) {
            this.mCachedPosts.add(post)
            postChangedNotification(listOf(this.mCachedPosts.lastIndex))
        } else {
            this.mCachedPosts.add(atIndex, post)
            postChangedNotification((atIndex..mCachedPosts.lastIndex).toList())
        }

    }

    fun replacePost(post: Post, withPost: Post) {
        val index = this.mCachedPosts.indexOf(post)
        if (index >= 0) {
            this.mCachedPosts[index] = withPost
            postChangedNotification(listOf(index))
        }
    }

    fun removePost(post: Post) {
        val index = this.mCachedPosts.indexOf(post)
        this.mCachedPosts.remove(post)
        postChangedNotification(if (index == -1) emptyList() else (index..this.mCachedPosts.lastIndex).toList())
    }

    fun addPosts(posts: Posts) {
        if (posts.isEmpty()) {
            return
        }
        val nextIndex = this.mCachedPosts.lastIndex + 1
        this.mCachedPosts.addAll(posts)
        postChangedNotification((nextIndex..this.mCachedPosts.lastIndex).toList())
    }

    fun reset(posts: Posts) {
        this.mCachedPosts.clear()
        this.mCachedPosts.addAll(posts)
        postChangedNotification(emptyList())
    }

    fun reset() {
        this.mCachedPosts.clear()
        postChangedNotification(emptyList())
    }

    fun posts(user: User? = null): Posts {
        return if (user != null) this.mCachedPosts.filter { post -> post.userId == user.id } else this.mCachedPosts.toMutableList()
    }

    fun postsCount(user: User? = null): Int {
        return if (user != null) this.mCachedPosts.count { post -> post.userId == user.id } else this.mCachedPosts.count()
    }

    fun post(id: Int): Post? {
        return this.mCachedPosts.find { it.id == id }
    }
}