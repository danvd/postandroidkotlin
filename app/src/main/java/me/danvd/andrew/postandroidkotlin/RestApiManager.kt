package me.danvd.andrew.postandroidkotlin

import  com.github.kittinunf.fuel.core.Method
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.core.Request
import com.github.kittinunf.fuel.httpDelete
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.fuel.httpPut
import com.github.kittinunf.result.Result
import io.reactivex.*
import kotlinx.serialization.Serializable
import kotlinx.serialization.internal.ArrayListSerializer
import kotlinx.serialization.json.*
import java.lang.Exception

typealias StringResultOrError = SimpleEither<StringError, String>
typealias TypeResultOrError<T> = SimpleEither<StringError, T>
typealias PostsOrError = SimpleEither<StringError, Posts>
typealias PostOrError = SimpleEither<StringError, Post>

class RestApiManager {

    enum class SortingDirection {
        ASC, DESC
    }

    // NOTE: The order of variable declarations is important for init{}
    val cache = PostsCollectionCache()
    var basePath: String?
        get() {
            return FuelManager.instance.basePath
        }
        set(basePath) {
            FuelManager.instance.basePath = basePath
            clearCache()
        }

    init {
        this.basePath = PostsApp.context().resources.getString(R.string.defaultURI)
    }

    var sorting: SortingDirection
        set(direction) {
            sortingDirection = direction
            clearCache()
        }
        get() {
            return sortingDirection
        }

    companion object {
        val shared = RestApiManager()
        // NOTE: default sorting is descending. Recent posts are displayed at top of the list
        private var sortingDirection = SortingDirection.DESC

        private fun sortingParameter(): String {
            return "_sort=id&_order=" + if (this.sortingDirection == SortingDirection.ASC) "asc" else "desc"
        }

        fun allPostsEndpoint(): String {
            return "/posts?" + sortingParameter()
        }

        fun allPostsUnsortedEndpoint(): String {
            return "/posts"
        }

        fun userPostsEndpoint(userId: Int): String {
            return "/posts?_userId=%d".format(userId) + sortingParameter()
        }

        fun slicePostsEndpoint(start: Int, limit: Int): String {
            return "/posts?_start=%d&_limit=%d&".format(start, limit) + sortingParameter()
        }

        fun onePostEndpoint(id: Int): String {
            return "/posts/%d".format(id)
        }

    }

    private fun clearCache() {
        this.cache.reset()
    }


    private fun <T> jsonGenericMethod(
        req: Request,
        param: String? = null,
        handler: (StringResultOrError) -> TypeResultOrError<T>
    ): Single<TypeResultOrError<T>> {
        return Single.create { emitter ->
            if (!emitter.isDisposed) { // Handle disposition during long-running network io
                if (param != null) {
                    req.header("Content-Type" to "application/json").body(param)
                }
                req.responseString { _, _, result ->
                    when (result) {
                        is Result.Failure -> {
                            val ex = result.getException()
                            emitter.tryOnError(Throwable(ex.message))
                        }
                        is Result.Success -> {
                            val data = result.get()
                            val typeResultOrError = handler(StringResultOrError.makeRight(data))
                            if (typeResultOrError.isRight()) {
                                emitter.onSuccess(typeResultOrError)
                            } else {
                                emitter.tryOnError(Throwable(typeResultOrError.left?.get()))
                            }
                        }
                    }
                }
            }
        }
    }

    private fun deserializePosts(
        resultOrError: StringResultOrError,
        resetCache: Boolean = true
    ): PostsOrError {
        val postsOrError: PostsOrError = PostsOrError.makeLeft(StringError())
        if (resultOrError.isRight()) {
            val jsonData = resultOrError.right
            if (jsonData != null) {
                try {
                    val posts = Json(JsonConfiguration.Stable).parse(
                        ArrayListSerializer(Post.serializer()),
                        jsonData
                    )
                    postsOrError.right = posts
                    if (resetCache) {
                        this.cache.reset(posts)
                    }
                } catch (e: Exception) {
                    postsOrError.left = StringError(e.localizedMessage)
                }
            }
        } else {
            postsOrError.left = resultOrError.left
        }
        if (postsOrError.isLeft()) {
            this.cache.reset()
        }
        return postsOrError
    }

    fun fetchAllPosts(): Single<PostsOrError> {
        return jsonGenericMethod(
            allPostsEndpoint().httpGet()
        ) { resultOrError ->
            deserializePosts(resultOrError)
        }
    }

    fun fetchAllPostsForUser(user: User): Single<PostsOrError> {
        return jsonGenericMethod(
            userPostsEndpoint(user.id).httpGet()
        ) { resultOrError ->
            deserializePosts(resultOrError)
        }
    }

    // New post. Let server do the work on assigning new id
    @Serializable
    data class NewPost(val title: String, val body: String, val userId: Int)


    private fun postToJsonString(post: Post): StringResultOrError {
        return try {
            StringResultOrError.makeRight(

                (if (post.isNew)
                    Json(JsonConfiguration.Stable).toJson(
                        NewPost.serializer(),
                        NewPost(post.title, post.body, post.userId)
                    )
                else
                    Json(JsonConfiguration.Stable).toJson(
                        Post.serializer(),
                        post
                    )).toString()
            )
        } catch (e: Exception) {
            StringResultOrError.makeLeft(StringError(e.localizedMessage))
        }
    }

    private fun singlePostOperation(post: Post, request: Request): Single<PostOrError> {
        val isDelete = request.method == Method.DELETE
        val isUpdate = request.method == Method.PUT
        var postJsonString: String? = null
        if (!isDelete) {
            val jsonStringOrError = postToJsonString(post)
            postJsonString = jsonStringOrError.right
            if (jsonStringOrError.isLeft() || postJsonString == null) {
                return Single.create { emitter ->
                    emitter.tryOnError(Throwable(jsonStringOrError.left?.get()))
                }
            }
        }

        return jsonGenericMethod(request, postJsonString) { stringOrError ->
            val jsonData = stringOrError.right
            val postOrError =
                PostOrError.makeLeft<StringError, Post>(StringError(stringOrError.left?.get()))
            if (jsonData != null) {
                if (isDelete) {
                    this.cache.removePost(post)
                    postOrError.right = post
                } else {
                    try {
                        val newPost =
                            Json(JsonConfiguration.Stable).parse(Post.serializer(), jsonData)
                        // replace or insert new post based on sorting
                        if (isUpdate) this.cache.replacePost(post, newPost) else
                            this.cache.addPost(
                                newPost,
                                if (this.sorting == SortingDirection.ASC) null else 0
                            )
                        postOrError.right = newPost
                    } catch (e: Exception) {
                        postOrError.left = StringError(e.localizedMessage)
                    }
                }
            }
            postOrError
        }
    }

    fun addPost(
        title: String,
        body: String,
        user: User = User.defaultUser()
    ): Single<PostOrError> {
        return singlePostOperation(
            Post(title, body, user),
            allPostsUnsortedEndpoint().httpPost()
        )
    }

    fun deletePost(post: Post): Completable {
        return singlePostOperation(post, onePostEndpoint(post.id).httpDelete()).ignoreElement()
    }

    fun updatePost(post: Post): Completable {
        return singlePostOperation(post, onePostEndpoint(post.id).httpPut()).ignoreElement()
    }

}