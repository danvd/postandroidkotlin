package me.danvd.andrew.postandroidkotlin

import android.app.Activity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers


class PostActivity : AppCompatActivity() {

    // Default action is to create new post
    private var mPostId: Int? = null

    private lateinit var mPostTitleView: EditText
    private lateinit var mPostMessageView: EditText
    private lateinit var mDisposable: Disposable

    private lateinit var mButtonSave: Button
    private lateinit var mButtonCancel: Button
    private lateinit var mButtonDelete: Button

    companion object {
        var postKey: String = "postKey"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post)

        mPostId = intent.extras?.get(postKey) as? Int
        val postId = mPostId

        val post = if (postId != null) RestApiManager.shared.cache.post(postId) else null


        // Init view references
        mPostTitleView = findViewById(R.id.post_title)
        mPostMessageView = findViewById(R.id.post_message)
        if (post != null) {
            mPostTitleView.setText(post.title)
            mPostMessageView.setText(post.body)
        }

        // Cancel button handler
        mButtonCancel = findViewById(R.id.btn_cancel)
        mButtonCancel.setOnClickListener { finish() }

        // Save button handler
        mButtonSave = findViewById(R.id.btn_save)
        mButtonSave.setOnClickListener { savePost() }

        // Delete button handler
        mButtonDelete = findViewById(R.id.btn_delete)
        mButtonDelete.setOnClickListener { deletePost() }

        findViewById<CardView>(R.id.btn_delete_holder).visibility =
            if (mPostId == null) View.GONE else View.VISIBLE
    }

    private fun validateEditText(editText: EditText): Boolean {
        val isOk = !editText.text.isBlank()
        editText.error = if (isOk) null else getString(R.string.error_empty_field)
        return isOk
    }

    private fun showError(error: String?) {
        Toast.makeText(this, error ?: "Unknown error", Toast.LENGTH_LONG).show()
    }

    private fun controlButtons(enable: Boolean) {
        mButtonDelete.isEnabled = enable
        mButtonCancel.isEnabled = enable
        mButtonSave.isEnabled = enable
    }

    private fun onCompletable(c: Completable) {
        mDisposable =
            c.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onComplete = {
                        setResult(Activity.RESULT_OK)
                        finish()
                    },
                    onError = { error ->
                        controlButtons(true)
                        showError(error.localizedMessage)
                    }
                )
    }

    private fun deletePost() {
        val postId = mPostId ?: return
        controlButtons(false)
        onCompletable(Post(postId).delete())
    }

    private fun savePost() {
        if (!validateEditText(mPostTitleView) || !validateEditText(mPostMessageView)) {
            return
        }

        val postId = mPostId
        val title = mPostTitleView.text.toString()
        val body = mPostMessageView.text.toString()
        val post = if (postId != null) Post(
            postId,
            title,
            body,
            User.defaultUser()
        ) else Post(
            title,
            body,
            User.defaultUser()
        )

        controlButtons(false)
        onCompletable(if (post.isNew) post.addAsNew() else post.update())
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::mDisposable.isInitialized) {
            mDisposable.dispose()
        }
    }

}