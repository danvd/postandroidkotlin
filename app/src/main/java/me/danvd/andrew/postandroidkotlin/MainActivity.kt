package me.danvd.andrew.postandroidkotlin

import android.animation.Animator
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatDelegate
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.rxkotlin.subscribeBy
import java.lang.Exception

class MainActivity : AppCompatActivity() {

    private lateinit var mDisposables: CompositeDisposable
    private lateinit var mDisposableIoObserver: Disposable
    private lateinit var mErrorView: TextView
    private lateinit var mErrorViewButton: Button
    private lateinit var mUpdateButton: ImageButton
    private lateinit var mErrorLayout: LinearLayout
    private lateinit var mProgressLayout: LinearLayout

    private lateinit var mPostsListView: RecyclerView
    private lateinit var mPostsListAdapter: RecyclerView.Adapter<*>
    private lateinit var mPostsListLayoutManager: LinearLayoutManager

    private var mAnimationDuration: Int = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Use system's default day/night setting on latest android devices.
        // Default mode is MODE_NIGHT_UNSPECIFIED which turns night mode explicitly.
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)

        setContentView(R.layout.activity_main)

        // Init view variables
        mErrorView = findViewById(R.id.error_view)
        mErrorViewButton = findViewById(R.id.error_view_button)
        mErrorLayout = findViewById(R.id.error_layout)
        mProgressLayout = findViewById(R.id.progress_layout)
        mPostsListView = findViewById(R.id.posts_list_view)
        mUpdateButton = findViewById(R.id.btn_update)

        // Setup list view adapter

        mPostsListLayoutManager = LinearLayoutManager(this)

        mPostsListAdapter = PostsListViewAdapter(this)

        mPostsListView.apply {
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            setHasFixedSize(true)

            // use a linear layout manager
            layoutManager = mPostsListLayoutManager

            // set an viewAdapter
            adapter = mPostsListAdapter

        }

        // Just in case hide error and progress views
        mErrorLayout.visibility = View.GONE
        mProgressLayout.visibility = View.GONE

        // RxKotlin observables to be disposed
        mDisposables = CompositeDisposable()

        // Retrieve and cache the system's default "short" animation time.
        mAnimationDuration = resources.getInteger(android.R.integer.config_mediumAnimTime)

        // In case of error provide ability to restart fetching process
        mErrorViewButton.setOnClickListener { fetchPosts() }

        // Add post button click handler
        findViewById<ImageButton>(R.id.btn_add_post).setOnClickListener {
            val addPostIndent = Intent(this, PostActivity::class.java)
            startActivity(addPostIndent)
        }

        // Update button handler
        mUpdateButton.setOnClickListener { fetchPosts() }

        mDisposables.add(RestApiManager.shared.cache.notifications()
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onNext = { (_, indices) ->
                    // In real app all notifyItem* partial cases should be processed based on indices
                    mPostsListAdapter.notifyDataSetChanged()
                    if (indices.isNotEmpty()) {
                        try {
                            val firstIndex = indices.first()
                            if (mPostsListLayoutManager.itemCount > firstIndex &&
                                !(mPostsListLayoutManager.findFirstCompletelyVisibleItemPosition()
                                        ..mPostsListLayoutManager.findLastCompletelyVisibleItemPosition()).contains(
                                    firstIndex
                                )
                            ) {
                                mPostsListLayoutManager.scrollToPositionWithOffset(
                                    firstIndex,
                                    10
                                )
                            }
                        } catch (e: Exception) {
                            println("Possible race condition in posts view")
                        }
                    }

                },
                onError = {
                    mPostsListAdapter.notifyDataSetChanged()
                }
            ))

        // Fetch for the first time
        fetchPosts()
    }

    private fun animateViewVisibility(view: View, show: Boolean) {
        val currentAnimation = view.animation
        // NOTE: currentAnimation is initially null
        if (currentAnimation != null && !currentAnimation.hasEnded()) {
            currentAnimation.cancel()
            currentAnimation.reset()
        }
        view.apply {
            alpha = if (!show) 1f else 0f

            animate()
                .alpha(1f - alpha)
                .setDuration(mAnimationDuration.toLong())
                .setListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(p0: Animator?) {
                        visibility = View.VISIBLE
                    }

                    override fun onAnimationCancel(p0: Animator?) {
                        onAnimationEnd(p0)
                    }

                    override fun onAnimationEnd(p0: Animator?) {
                        visibility = if (!show) View.GONE else View.VISIBLE
                    }

                    override fun onAnimationRepeat(p0: Animator?) {
                        visibility = View.VISIBLE
                    }
                })
        }
    }

    private fun controlProgress(show: Boolean) {
        if (show) {
            controlError()
        }
        animateViewVisibility(mProgressLayout, show)
    }

    private fun controlError(error: String? = null) {
        val show = error != null
        if (show) {
            controlProgress(false)
        }
        mErrorView.text = error
        animateViewVisibility(mErrorLayout, show)
    }

    private fun controlButtons(enable: Boolean) {
        mErrorViewButton.isEnabled = enable
    }

    private fun fetchPosts() {
        controlError()
        controlProgress(true)
        if (::mDisposableIoObserver.isInitialized) {
            mDisposableIoObserver.dispose()
        }
        controlButtons(false)
        mDisposableIoObserver =
            RestApiManager.shared.fetchAllPosts()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onError = {
                        // Explicitly clear cache on error
                        RestApiManager.shared.cache.reset()
                        controlError(it.localizedMessage)
                        controlButtons(true)
                    },
                    onSuccess = {
                        controlProgress(false)
                        // At this point posts are cached, so out UI can be updated
                        // This is done through posts cache notifications
                        controlButtons(true)
                    }
                )
    }

    override fun onDestroy() {
        super.onDestroy()
        mDisposableIoObserver.dispose()
        mDisposables.dispose()
    }
}
