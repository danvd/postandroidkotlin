package me.danvd.andrew.postandroidkotlin

import io.reactivex.Completable
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient

@Serializable
class Post(
    val id: Int,
    var title: String = "",
    var body: String = "",
    val userId: Int = User.defaultUser().id
) {
    @Transient
    var isNew: Boolean = false

    constructor(
        title: String,
        body: String,
        user: User = User.defaultUser()
    ) : this(0, title, body, user) {
        this.isNew = true
    }

    override operator fun equals(other: Any?): Boolean {
        return other is Post && other.id == this.id
    }

    override fun hashCode(): Int {
        return this.id
    }

    constructor(id: Int, title: String, body: String, user: User) :
            this(id, title, body, user.id)

    fun delete(): Completable {
        return RestApiManager.shared.deletePost(this)
    }

    fun update(): Completable {
        return RestApiManager.shared.updatePost(this)
    }

    fun user(): User {
        return User(this.userId)
    }

    fun addAsNew(): Completable {
        return RestApiManager.shared.addPost(this.title, this.body, this.user()).ignoreElement()
    }
}