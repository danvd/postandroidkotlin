package me.danvd.andrew.postandroidkotlin

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView


class PostsListViewAdapter(private val context: Context) :
    RecyclerView.Adapter<PostsListViewAdapter.PostViewHolder>() {

    private val mPosts: List<Post>
        get() {
            return RestApiManager.shared.cache.posts()
        }

    class PostViewHolder(itemRowView: LinearLayout, private val activityContext: Context) :
        RecyclerView.ViewHolder(itemRowView), View.OnClickListener {
        private var mPostTitleView = itemRowView.findViewById<TextView>(R.id.post_title)
        private val mPostMessageView = itemRowView.findViewById<TextView>(R.id.post_message)
        private val mPostIndexView = itemRowView.findViewById<TextView>(R.id.post_index)
        private val mPostComboBoxView = itemRowView.findViewById<CardView>(R.id.post_combo_box)

        override fun onClick(v: View?) {
            if (v == null || postId <= 0) {
                return
            }
            activityContext.apply {
                val editPostIntent = Intent(this, PostActivity::class.java)
                val b = Bundle()
                b.putInt(PostActivity.postKey, postId)
                editPostIntent.putExtras(b)
                startActivity(editPostIntent)
            }
        }

        init {
            mPostComboBoxView.setOnClickListener(this)
        }

        var title: String
            set(title) {
                mPostTitleView.text = title
            }
            get() {
                return mPostTitleView.text.toString()
            }

        var message: String
            set(message) {
                mPostMessageView.text = message
            }
            get() {
                return mPostMessageView.text.toString()
            }
        var index: Int
            set(index) {
                mPostIndexView.text = index.toString()
            }
            get() {
                return mPostIndexView.text.toString().toInt(10)
            }
        var postId: Int = -1

    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): PostViewHolder {
        // create a new view
        val itemRowView = LayoutInflater.from(parent.context)
            .inflate(R.layout.post_item_row, parent, false) as LinearLayout
        return PostViewHolder(itemRowView, context)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        // - get element from the dataset at given position
        // - replace the contents of the view with that element
        val post = mPosts[position]
        holder.index = position + 1
        holder.title = post.title
        holder.message = post.body
        holder.postId = post.id
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = mPosts.size
}