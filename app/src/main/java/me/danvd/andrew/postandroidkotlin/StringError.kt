package me.danvd.andrew.postandroidkotlin

class StringError(private val error: String? = null) {

    fun get(): String {
        return error ?: "Generic error"
    }
}