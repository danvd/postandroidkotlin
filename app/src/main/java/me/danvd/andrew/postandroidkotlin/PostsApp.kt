package me.danvd.andrew.postandroidkotlin

import android.app.Application
import android.content.Context

class PostsApp : Application() {

    companion object {
        private lateinit var context: Context

        fun context(): Context {
            return context
        }
    }

    override fun onCreate() {
        super.onCreate()
        context = this
    }


}