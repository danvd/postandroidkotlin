package me.danvd.andrew.postandroidkotlin

import io.reactivex.Single

class User(val id: Int) {

    val posts: Single<PostsOrError>
        get() {
            return RestApiManager.shared.fetchAllPostsForUser(this)
        }

    fun addPost(title: String, body: String): Single<PostOrError> {
        return RestApiManager.shared.addPost(title, body, this)
    }

    companion object {
        fun defaultUser(): User {
            return User(1)
        }
    }
}