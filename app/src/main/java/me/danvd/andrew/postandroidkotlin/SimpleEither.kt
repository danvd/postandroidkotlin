package me.danvd.andrew.postandroidkotlin

class SimpleEither<L, R> internal constructor(left: L?, right: R?) {
    var left: L? = left
        set(left) {
            if (field == left) {
                return
            }
            this.right = null
            field = left
        }
    var right: R? = right
        set(right) {
            if (field == right) {
                return
            }
            this.left = null
            field = right
        }

    fun isLeft(): Boolean {
        return this.left != null
    }

    fun isRight(): Boolean {
        return this.right != null
    }

    companion object {
        fun <L, R> makeLeft(left: L): SimpleEither<L, R> {
            return SimpleEither(left, null)
        }

        fun <L, R> makeRight(right: R): SimpleEither<L, R> {
            return SimpleEither(null, right)
        }
    }


}